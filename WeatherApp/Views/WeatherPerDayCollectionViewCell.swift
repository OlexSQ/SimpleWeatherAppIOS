//
//  WeatherPerDayCollectionViewCell.swift
//  WeatherApp
//
//  Created by Mac on 28.04.2021.
//

import UIKit

class WeatherPerDayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var degreesLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var imageURL: URL? {
        didSet {
            weatherImageView?.image = nil
            updateUI()
        }
    }
    
    private func updateUI() {
        if let url = imageURL {
            spinner.isHidden = false
            spinner?.startAnimating()
            DispatchQueue.global(qos: .userInitiated).async {
                let contentsOfURL = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if url == self.imageURL {
                        if let imageData = contentsOfURL {
                            self.weatherImageView?.image = UIImage(data: imageData)
                        } 
                        self.spinner?.stopAnimating()
                        self.spinner.isHidden = true
                    }
                }
            }
        }
    }
}

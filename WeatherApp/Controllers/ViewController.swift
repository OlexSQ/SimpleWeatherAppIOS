//
//  ViewController.swift
//  WeatherApp
//
//  Created by Mac on 26.04.2021.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var degreesLabel: UILabel!
    
    let mySerialQueue = DispatchQueue(label: "com.getPic.mySerial")
    let myConcurrentQueue = DispatchQueue(label: "com.getPic.mySerial", qos: .userInitiated, attributes: .concurrent)
    var weatherPerDay = [WeatherPerDay]()
    var imageURLs = [String]()
    
    var city = ""
    let urlForFirstRequest = "http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=e83bce43f40758140ef6927fda5cfc85&units=metric&q=Kropyvnytskyi"
    
    var urlString = "http://api.openweathermap.org/data/2.5/forecast?id=524901&appid=e83bce43f40758140ef6927fda5cfc85&units=metric"
    var urlImage = "http://openweathermap.org/img/wn/"
    var weatherData: WeatherData?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        collectionView.delegate = self
        collectionView.dataSource = self
        setSearhBarView()
        getData(from: urlForFirstRequest) {
            DispatchQueue.main.async {
                self.refreshUI()
                self.getDataToAllDay()
            }
        }
        
    }
    
    //MARK: - GetData
    func getData(from urlString: String, comletionHandler: @escaping () -> ()) {
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
        
            guard let data = data else { return }
            guard error == nil else { return }
        
            do {
                let weatherInfo = try JSONDecoder().decode(WeatherData.self, from: data)
                self.weatherData = weatherInfo
                print("We got all data. 🤪")
                let imageUrl = self.urlImage + (self.weatherData?.list.first?.weather.first?.icon ?? "01d") + "@2x.png"
                self.weatherImageView.downloaded(from: imageUrl) {
                    print("Main image downloaded success")
                }
                
                if (self.weatherData?.cod != "200") {
                    self.showAlert(title: "Opps", message: "Maybe somthing goes wrang")
                }
                
            } catch let error {
                self.showAlert(title: "Opps", message: "Be carefuly, you got error")
                print("We can't get data 😢 \(error)")
            }
            
            comletionHandler()
                    
        }.resume()

    }
    
    //MARK: - GetAllDataTo5DayForecast
    func getDataToAllDay() {
        let timer = Timer()
        if let list = self.weatherData?.list {
            self.mySerialQueue.sync {
                //var indexForImage = 0
                for forecast in list {
                    if (forecast.dtTxt.contains("12:00")) {
                        let index = forecast.dtTxt.index(forecast.dtTxt.startIndex, offsetBy: 10)
                        let date = String(forecast.dtTxt.prefix(upTo: index)) as String
                        let imageUrl = self.urlImage + (forecast.weather.first?.icon ?? "01d") + "@2x.png"
                        self.imageURLs.append(imageUrl)
                        let weather = WeatherPerDay(date: date, temp: Int(forecast.main.temp), desc: forecast.weather.first?.main.rawValue ?? "Not so bad")
                            self.weatherPerDay.append(weather)


//                        weather.weatherImageView.downloaded(from: imageUrl) {
//                            print("Downloaded \(indexForImage) image")
//                            self.weatherPerDay.append(weather)
//                            indexForImage += 1
//                        }
                    }
                }
                DispatchQueue.main.async(flags: .barrier) {
                    self.collectionView.reloadData()
                    print("Operation time is \(timer.stop())")
                }
            }
            
        }
    }
    
    //MARK: - RefreshUI
    func refreshUI() {
        self.cityLabel.text = self.weatherData?.city.name ?? "The best place"
        self.descriptionLabel.text = self.weatherData?.list.first?.weather.first?.main.rawValue
        self.degreesLabel.text = "\(Int(self.weatherData?.list.first?.main.temp ?? 28.0))" + "°"
        self.collectionView.reloadData()
    }
    
    //MARK: - Alert
    func createAlertAction(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            print("Action")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            self.createAlertAction(title:
                                    title, message: message)
        }
    }
    
    
    
    
    //MARK: - SearchBar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var urlString = ""
        self.weatherPerDay = []
        self.imageURLs = []
        searchBar.resignFirstResponder()
        if !(searchBar.searchTextField.text?.isEmpty ?? true) {
            city = searchBar.searchTextField.text ?? "Kropyvnytskyi"
            urlString = self.urlString + "&q=" + city
            print("URLString is \(urlString)")
            
            getData(from: urlString) {
                DispatchQueue.main.async {
                    self.refreshUI()
                    self.getDataToAllDay()
                }
            }
            searchBar.searchTextField.text = ""
        } else {
            print("Nothing entered")
        }

    }
    
    func setSearhBarView() {
        searchBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        searchBar.searchTextField.backgroundColor = .white
    }
    
    //MARK: - CollectionView methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherPerDay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! WeatherPerDayCollectionViewCell
        if !(self.weatherPerDay.isEmpty) {
            cell.dateLabel.text = self.weatherPerDay[indexPath.row].date
            cell.descriptionLabel.text = self.weatherPerDay[indexPath.row].desc
            cell.degreesLabel.text = "\(self.weatherPerDay[indexPath.row].temp)°"
            cell.imageURL =  URL(string: imageURLs [indexPath.row])
            //cell.weatherImageView.image = self.weatherPerDay[indexPath.row].weatherImageView.image
        }
                
        return cell
    }


}

//MARK: - Extension
extension UIImageView {
    func downloaded(from url: URL) {
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                // Set image with animation
                self?.setImage(image)
                
                
                // Set image without animation
                // self.image? = image
                
            }
        }.resume()
    }
    func downloaded(from link: String, comletionHandler: @escaping () -> ()) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url)
        comletionHandler()
    }
}

extension UIImageView {
    func setImage(_ image: UIImage?, animated: Bool = true) {
        let duration = animated ? 1.5 : 0.0
        UIView.transition(with: self, duration: duration, options: .transitionCrossDissolve, animations: {
            self.image = image
        }, completion: nil)
    }
}


